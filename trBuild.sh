#!/bin/bash
rm -r TR-PRSD
mkdir TR-PRSD
wget https://sites.google.com/a/wmail.fi/greeknt/home/greeknt/TR-PRSD.ZIP
unzip TR-PRSD.ZIP -d TR-PRSD/

cd TR-PRSD
sed -ri 's/<baqeov> 901/baqeov 901/g' JOH.UTR
sed -ri 's/M6: <monov 3441 \{A-NSM\}/M6:/g' JOH.UTR
sed -ri 's/o 3588 \{T-NSM\} ihsouv> 2424 \{N-NSM\} VAR:/ VAR:/g' JOH.UTR
sed -ri 's/\r//' *UTR
sed -ri 's/^1:1( paulov 3972 \{N-NSM\} klhtov 2822 \{A-NSM\} apostolov 652)/\\id 1CO\n\\c 1\n\\p\n\\v 1\1/g' *UTR
sed -ri 's/^1:1( o 3739 \{R-NSN\} hn 1510)/\\id 1JN\n\\c 1\n\\p\n\\v 1\1/g' *UTR
sed -ri 's/^1:1( petrov 4074 \{N-NSM\} apostolov 652 \{N-NSM\} ihsou 2424)/\\id 1PE\n\\c 1\n\\p\n\\v 1\1/g' *UTR
sed -ri 's/^1:1( paulov 3972 \{N-NSM\} kai 2532 \{CONJ\} silouanov 4610)/\\id 1TH\n\\c 1\n\\p\n\\v 1\1/g' 1TH.UTR
sed -ri 's/^1:1( paulov 3972 \{N-NSM\} kai 2532 \{CONJ\} silouanov 4610)/\\id 2TH\n\\c 1\n\\p\n\\v 1\1/g' 2TH.UTR
sed -ri 's/^1:1( paulov 3972 \{N-NSM\} apostolov 652 \{N-NSM\} ihsou 2424)/\\id 1TI\n\\c 1\n\\p\n\\v 1\1/g' 1TI.UTR
sed -ri 's/^1:1( paulov 3972 \{N-NSM\} apostolov 652 \{N-NSM\} ihsou 2424)/\\id  2CO\n\\c 1\n\\p\n\\v 1\1/g' 2CO.UTR
sed -ri 's/^1:1( o 3588 \{T-NSM\} presbuterov 4245 \{A-NSM-C\} eklekth 1588)/\\id 2JN\n\\c 1\n\\p\n\\v 1\1/g' *UTR
sed -ri 's/^1:1( \| sumewn 4826 \{N-PRI\} \| )/\\id 2PE\n\\c 1\n\\p\n\\v 1\1/g' *UTR
sed -ri 's/^1:1( paulov 3972 \{N-NSM\} kai 2532 \{CONJ\} silouanov 4610)/\\id 2TH\n\\c 1\n\\p\n\\v 1\1/g' *UTR
sed -ri 's/^1:1( paulov 3972 \{N-NSM\} apostolov 652 \{N-NSM\} ihsou 2424)/\\id 2TI\n\\c 1\n\\p\n\\v 1\1/g' 2TI.UTR
sed -ri 's/^1:1( o 3588 \{T-NSM\} presbuterov 4245 \{A-NSM-C\} gaiw 1050)/\\id 3JN\n\\c 1\n\\p\n\\v 1\1/g' *UTR
sed -ri 's/^1:1( ton 3588 \{T-ASM\} men 3303 \{PRT\} prwton 4413)/\\id ACT\n\\c 1\n\\p\n\\v 1\1/g' *UTR
sed -ri 's/^1:1( paulov 3972 \{N-NSM\} apostolov 652 \{N-NSM\} ihsou 2424)/\\id COL\n\\c 1\n\\p\n\\v 1\1/g' COL.UTR
sed -ri 's/^1:1( paulov 3972 \{N-NSM\} apostolov 652 \{N-NSM\} ihsou 2424)/\\id EPH\n\\c 1\n\\p\n\\v 1\1/g' EPH.UTR
sed -ri 's/^1:1( paulov 3972 \{N-NSM\} apostolov 652 \{N-NSM\} ouk 3756)/\\id GAL\n\\c 1\n\\p\n\\v 1\1/g' *UTR
sed -ri 's/^1:1( polumerwv 4181 \{ADV\} kai 2532 \{CONJ\} polutropwv 4187)/\\id HEB\n\\c 1\n\\p\n\\v 1\1/g' *UTR
sed -ri 's/^1:1( iakwbov 2385 \{N-NSM\} qeou 2316 \{N-GSM\} kai 2532)/\\id JAS\n\\c 1\n\\p\n\\v 1\1/g' *UTR
sed -ri 's/^1:1( en 1722 \{PREP\} arch 746 \{N-DSF\} hn 1510)/\\id JHN\n\\c 1\n\\p\n\\v 1\1/g' *UTR
sed -ri 's/^1:1( ioudav 2455 \{N-NSM\} ihsou 2424 \{N-GSM\} cristou 5547)/\\id JUD\n\\c 1\n\\p\n\\v 1\1/g' *UTR
sed -ri 's/^1:1( epeidhper 1895 \{CONJ\})/\\id LUK\n\\c 1\n\\p\n\\v 1\1/g' *UTR
sed -ri 's/^1:1( arch 746 \{N-NSF\} tou 3588 \{T-GSN\} euaggeliou 2098)/\\id MRK\n\\c 1\n\\p\n\\v 1\1/g' *UTR
sed -ri 's/^1:1( biblov 976 \{N-NSF\} genesewv 1078 \{N-GSF\} ihsou 2424)/\\id MAT\n\\mt Textus Receptus \(1550\/1894\)\n\\c 1\n\\p\n\\v 1\1/g' *UTR
sed -ri 's/^1:1( paulov 3972 \{N-NSM\} desmiov 1198 \{N-NSM\} cristou 5547)/\\id PHM\n\\c 1\n\\p\n\\v 1\1/g' *UTR
sed -ri 's/^1:1( paulov 3972 \{N-NSM\} kai 2532 \{CONJ\} timoqeov 5095)/\\id PHP\n\\c 1\n\\p\n\\v 1\1/g' *UTR
sed -ri 's/^1:1( apokaluyiv 602 \{N-NSF\} ihsou 2424 \{N-GSM\} cristou 5547)/\\id REV\n\\c 1\n\\p\n\\v 1\1/g' *UTR
sed -ri 's/^1:1( paulov 3972 \{N-NSM\} doulov 1401 \{N-NSM\} ihsou 2424)/\\id ROM\n\\c 1\n\\p\n\\v 1\1/g' *UTR
sed -ri 's/^1:1( paulov 3972 \{N-NSM\} doulov 1401 \{N-NSM\} qeou 2316)/\\id TIT\n\\c 1\n\\p\n\\v 1\1/g' *UTR

#sed -ri 's/^\{/@@/g' *UTR


sed -ri 's/a/α/g' *UTR
sed -ri 's/b/β/g' *UTR
sed -ri 's/c/χ/g' *UTR
sed -ri 's/d/δ/g' *UTR
sed -ri 's/e/ε/g' *UTR
sed -ri 's/f/φ/g' *UTR
sed -ri 's/g/γ/g' *UTR
sed -ri 's/h/η/g' *UTR
sed -ri 's/i/ι/g' *UTR
sed -ri 's/k/κ/g' *UTR
sed -ri 's/l/λ/g' *UTR
sed -ri 's/m/μ/g' *UTR
sed -ri 's/n/ν/g' *UTR
sed -ri 's/o/ο/g' *UTR
sed -ri 's/p/π/g' *UTR
sed -ri 's/q/θ/g' *UTR
sed -ri 's/r/ρ/g' *UTR
sed -ri 's/s/σ/g' *UTR
sed -ri 's/t/τ/g' *UTR
sed -ri 's/u/υ/g' *UTR
sed -ri 's/v/ς/g' *UTR
sed -ri 's/w/ω/g' *UTR
sed -ri 's/x/ξ/g' *UTR
sed -ri 's/y/ψ/g' *UTR
sed -ri 's/z/ζ/g' *UTR
sed -ri 's/^\\χ 1/\\c 1/g' *UTR
sed -ri 's/^\\ιδ/\\id /g' *UTR
sed -ri 's/^\\ς 1 /\\v 1 /g' *UTR
sed -ri 's/^\\π/\\p/g' *UTR
sed -ri 's/  / /g' *UTR
sed -ri 's/μτ Tεξτυσ Rεχεπτυσ/mt Textus Receptus/g' *UTR
sed -ri 's/^([0-9]*):1 /\\c \1\n\\p\n\\v 1 /g' *UTR
sed -ri ':a;N;$!ba;s/\n( )/ \1/g' *UTR
sed -ri 's/([α-ω]*) (\| [α-ω]*) \| ([0-9]*) (\{[A-Z-]*\})/\1 \3 \4 \2 \3 \4 \| /g' *UTR
sed -ri 's/  / /g' *UTR
sed -ri 's/(συχεμ|ετυθη|ιδετε|συνεζωποιησεν|σαρωναν|ναζαρετ|περιπεπατηκει) (\| σιχεμ|\| εθυθη|\| ειδετε|\| συνεζωοποιησεν|\| σαρωνα|\| ναζαρεθ|\| περιεπεπατηκει) \| (4966|2380 5681|3708 5627|4806 5656|4565|3478|4043 5715) (\{N-PRI\}|\{V-API-3S\}|\{V-2AAI-2P\}|\{V-AAI-3S\}|\{N-ASM\}|\{V-LAI-3S\})/\1 \3 \4 \2 \3 \4 \|/g' *UTR


##Versets
sed -ri 's/^[0-9]*:([0-9]*) /\\v \1 /g' *UTR

#sed -ri 's/([α-ω]*) ([0-9]*) ([0-9]*) \{([A-Z0-9-]*)\}/\\w \1\|strong="G\2,G\3" x-morph="\4"\\w\*/g' *UTR
sed -ri 's/([α-ω]*) ([0-9]*) ([0-9]*) \{([A-Z0-9-]*)\}/\\w \1\|strong="G\2,G\3,\4"\\w\*/g' *UTR
#sed -ri 's/([α-ω]*) ([0-9]*) \{([A-Z0-9-]*)\}/ \\w \1\|strong="G\2" x-morph="\3"\\w\*/g' *UTR
sed -ri 's/([α-ω]*) ([0-9]*) \{([A-Z0-9-]*)\}/ \\w \1\|strong="G\2,\3"\\w\*/g' *UTR
#sed -ri 's/([α-ω]*) ([0-9]*) ([0-9]*) \{([A-Z0-9-]*)\}/\\w \1\|strong="G\2,G\3" x-morph="\4"\\w\*/g' *UTR
#sed -ri 's/([α-ω]*) ([0-9]*) \{([A-Z0-9-]*)\}/ \\w \1\|strong="G\2" x-morph="\3"\\w\*/g' *UTR
sed -ri 's/  / /g' *UTR
##Traiter les variants
sed -ri 's/ \| / \\var1 /' *UTR
sed -ri 's/ \| / \\var1\* \\var2 /' *UTR
sed -ri 's/ \| / \\var2\* /' *UTR
#Deux fois si besoin
sed -ri 's/ \| / \\var1 /' *UTR
sed -ri 's/ \| / \\var1\* \\var2 /' *UTR
sed -ri 's/ \| / \\var2\* /' *UTR
sed -ri 's/ \|$/ \\var2\*/' *UTR
rename 's/UTR/usfm/g' *UTR
##Réparation de recalcitrants
sed -ri 's/\| (\\w συχεμ\|strong="G4966,N-PRI"\\w\*) \|/\\var1 \1\\var1\* \\var2 /g' *usfm
sed -ri 's/\| (\\w ποιησωμεν\|strong="G4160,G5661,V-AAS-1P"\\w\*) \| (\\w ποιησομεν\|strong="G4160,G5692,V-FAI-1P"\\w\*) \|/\\var1 \1\\var1\* \\var2 \2\\var2\*/g' *usfm
sed -ri 's/\| (\\w εμπορευσωμεθα\|strong="G1710,G5667,V-ADS-1P"\\w\* \\w και\|strong="G2532,CONJ"\\w\* \\w κερδησωμεν\|strong="G2770,G5661,V-AAS-1P"\\w\*) \|/\\var1 \1\\var1\* \\var2 /g' *usfm
##Corrections according to https://tracker.crosswire.org/browse/MOD-349
sed -ri 's/( μου\|strong="G)1473/\13450/g' *usfm
sed -ri 's/( ρηθεν\|strong="G)2046/\14483/g' *usfm
sed -ri 's/( ιδου\|strong="G)3708/\12400/g' *usfm
sed -ri 's/( ιδων\|strong="G)3708/\11492/g' *usfm
sed -ri 's/( εστιν\|strong="G)1510/\12076/g' *usfm
sed -ri 's/( σου\|strong="G)4771/\14675/g' *usfm
 sed -ri 's/( ην\|strong="G)1510/\12258/g' *usfm
cd ../
u2o.py -e utf-8 -l grc -o tr.osis.xml -v -d TR -x TR-PRSD/*.usfm
sed -ri 's/ strong:([A-Z][A-Z1-3-]*">)/" morph="robinson:\1/g' *osis.xml
mkdir ~/.sword/modules/texts/ztext/trnv/
osis2mod ~/.sword/modules/texts/ztext/trnv/ tr.osis.xml -z
